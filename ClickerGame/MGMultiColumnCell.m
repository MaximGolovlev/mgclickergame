//
//  MGMultiColumnCell.m
//  ClickerGame
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "MGMultiColumnCell.h"
#import "MGLevelScore.h"

@interface MGMultiColumnCell ()

@property (weak, nonatomic) IBOutlet UILabel *levelColumn;
@property (weak, nonatomic) IBOutlet UILabel *scoreColumn;
@property (weak, nonatomic) IBOutlet UILabel *scorePerClickColumn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cellWidthConstraint;

@end

@implementation MGMultiColumnCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)fillWithData:(NSDictionary*)data forIndexPath:(NSIndexPath *)indexPath {
    
    self.cellWidthConstraint.constant = CGRectGetWidth([UIScreen mainScreen].bounds)/3;
    
    if (indexPath.row != 0) {
        NSDictionary *dict = [data[@"levels"] objectAtIndex:indexPath.row - 1];
        MGLevelScore *levelScore = [[MGLevelScore alloc] initWithResponse:dict];
        
        self.levelColumn.text = [NSString stringWithFormat:@"%ld", (long)levelScore.level];
        self.scoreColumn.text = [NSString stringWithFormat:@"%ld", (long)levelScore.score];
        self.scorePerClickColumn.text = [NSString stringWithFormat:@"%ld", (long)levelScore.scorePerClick];
        
    } else {
        self.levelColumn.text = @"Уровень";
        self.scoreColumn.text =  @"Количество очков для перехода на уровень";
        self.scorePerClickColumn.text = @"Количество очков за клик";
    }
}

@end
