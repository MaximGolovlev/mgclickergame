//
//  ViewController.m
//  ClickerGame
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "ViewController.h"
#import "MGScoreTableViewController.h"
#import "MGLevelScore.h"

NSString *const kPlistPath      = @"https://bitbucket.org/MaximGolovlev/mgclickergame/raw/2cc3c5e3c3c6e3328f638a0618759b856e9a6748/TableScore";
NSString *const kLevel          = @"level";
NSString *const kScore          = @"score";
NSString *const kButtonTitle    = @"buttonTitle";
NSString *const kCurrentScore   = @"currentScore";
NSString *const kClickCounter   = @"clickCounter";
NSString *const kCurrentLvl     = @"currentLvl";

@interface ViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *scoreLable;
@property (weak, nonatomic) IBOutlet UILabel *levelLable;
@property (weak, nonatomic) IBOutlet UIButton *getPointsButton;

@property (strong, nonatomic) NSMutableDictionary *tableScore;
@property (assign, nonatomic) NSInteger clickCounter;
@property (strong, nonatomic) NSMutableArray *arrayOfLevels;
@property (assign, nonatomic) NSInteger currentLvl;
@property (assign, nonatomic) NSInteger currentScore;
@property (strong, nonatomic) NSString *buttonTitle;

- (IBAction)actionShowTableScore:(UIButton *)sender;
- (IBAction)actionGetPoints:(UIButton *)sender;
- (IBAction)actionNewGame:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayOfLevels = [NSMutableArray array];
    [self loadTableScore];
    
    NSUserDefaults * myNSUserDefaults = [NSUserDefaults standardUserDefaults];
    NSInteger clickCounter = [myNSUserDefaults integerForKey:kClickCounter];
    if(clickCounter != 0){
        [self loadProgress];
    } else
        [self prepearForGame];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Methods

- (void) deleteProgress {
    [NSUserDefaults resetStandardUserDefaults];
    [NSUserDefaults standardUserDefaults];
}

- (void) loadProgress {
    NSUserDefaults * myNSUserDefaults = [NSUserDefaults standardUserDefaults];
    
    self.levelLable.text = [myNSUserDefaults stringForKey:kLevel];
    self.scoreLable.text = [myNSUserDefaults stringForKey:kScore];
    self.buttonTitle     = [myNSUserDefaults stringForKey:kButtonTitle];
    self.currentScore    = [myNSUserDefaults integerForKey:kCurrentScore];
    self.clickCounter    = [myNSUserDefaults integerForKey:kClickCounter];
    self.currentLvl      = [myNSUserDefaults integerForKey:kCurrentLvl];
    
    [self.getPointsButton setTitle:self.buttonTitle forState:UIControlStateNormal];
}

- (void) saveProgress {
    NSUserDefaults * myNSUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [myNSUserDefaults setObject:self.levelLable.text forKey:kLevel];
    [myNSUserDefaults setObject:self.scoreLable.text forKey:kScore];
    [myNSUserDefaults setObject:self.buttonTitle forKey:kButtonTitle];
    [myNSUserDefaults setInteger:self.currentScore forKey:kCurrentScore];
    [myNSUserDefaults setInteger:self.clickCounter forKey:kClickCounter];
    [myNSUserDefaults setInteger:self.currentLvl forKey:kCurrentLvl];
    [myNSUserDefaults synchronize];
}

- (void) prepearForGame {
    
    [self deleteProgress];
    self.levelLable.text = @"Level: 0";
    self.scoreLable.text = @"Score: 0";
    [self.getPointsButton setTitle:@"1" forState:UIControlStateNormal];
    self.clickCounter = 0;
    self.currentLvl = 0;
    self.currentScore = 0;
    self.buttonTitle = @"1";
}

- (void) getLevelsFromTableScoreList:(NSMutableDictionary*)list {
    
    for (NSDictionary *dict in list[@"levels"]) {
        
        MGLevelScore *level = [[MGLevelScore alloc] initWithResponse:dict];
        [self.arrayOfLevels addObject:level];
    }
}

- (void) loadTableScore {
    
    NSData *plistData = [NSData dataWithContentsOfURL:[NSURL URLWithString:kPlistPath]];
    if (!plistData) {
        NSLog(@"errer reading from file: %@", kPlistPath);
        return;
    }
    
    NSPropertyListFormat format;
    NSError *error = nil;
    id plist = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListMutableContainersAndLeaves format:&format error:&error];
    
    if (!error){
        NSMutableDictionary *root = plist;
        NSLog(@"loaded data:\n%@", root);
        [self getLevelsFromTableScoreList:plist];
        self.tableScore = plist;
        
    } else {
        NSLog(@"error: %@", error);
    }
}

-(void) animateScoreAdding {
    
    UILabel * scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(arc4random_uniform(self.view.frame.size.width-60), 200, 60, 20)];
    scoreLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:25];
    scoreLabel.textColor = [UIColor redColor];
    scoreLabel.textAlignment = NSTextAlignmentCenter;
    scoreLabel.alpha = 0;
    scoreLabel.text = [@"+" stringByAppendingString: self.buttonTitle ];
    [self.view addSubview: scoreLabel];
    
    [UIView animateWithDuration:0.3 animations:^{
        scoreLabel.alpha = 1;
    } completion:^(BOOL finished) {
         [UIView animateWithDuration:0.5 animations:^{
             scoreLabel.alpha = 0;
         } completion:^(BOOL finished) {
              [scoreLabel removeFromSuperview];
          }];
     }];
}

#pragma mark - Actions

- (IBAction)actionShowTableScore:(UIButton *)sender {
    
    MGScoreTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MGScoreTableViewController"];
    vc.tableScore = self.tableScore;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)actionGetPoints:(UIButton *)sender {
    
    MGLevelScore *currentLevel = [self.arrayOfLevels objectAtIndex:self.currentLvl];
    
    if (currentLevel.level < [self.arrayOfLevels count]) {
        MGLevelScore *nextLevel = [self.arrayOfLevels objectAtIndex:self.currentLvl + 1];
        
        self.clickCounter++;
        self.currentScore = self.currentScore + currentLevel.scorePerClick;
        
        if (self.currentScore >= nextLevel.score) {
            self.clickCounter = 0;
            self.currentLvl++;
        }
        self.scoreLable.text = [NSString stringWithFormat:@"Score: %ld", self.currentScore];
        self.levelLable.text = [NSString stringWithFormat:@"Level: %ld", [[self.arrayOfLevels objectAtIndex:self.currentLvl] level]];
        
        [self animateScoreAdding];
        self.buttonTitle = [NSString stringWithFormat:@"%ld", [[self.arrayOfLevels objectAtIndex:self.currentLvl] scorePerClick]];
        [sender setTitle: self.buttonTitle forState:UIControlStateNormal];
        
        [self saveProgress];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Game Over" message:@"Start Another Game?" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
}

- (IBAction)actionNewGame:(UIButton *)sender {
    
    [self prepearForGame];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self prepearForGame];
}

@end
