//
//  MGLevelScore.h
//  ClickerGame
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MGLevelScore : NSObject

@property(assign, nonatomic)NSInteger level;
@property(assign, nonatomic)NSInteger score;
@property(assign, nonatomic)NSInteger scorePerClick;

- (instancetype)initWithResponse:(NSDictionary*)response;

@end
