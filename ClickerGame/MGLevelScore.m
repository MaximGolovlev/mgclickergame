//
//  MGLevelScore.m
//  ClickerGame
//
//  Created by Admin on 26.08.15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "MGLevelScore.h"

@implementation MGLevelScore

- (instancetype)initWithResponse:(NSDictionary*)response {
    
    self = [super init];
    if (self) {
        
        self.level = [response[@"number"] integerValue];
        self.score = [response[@"score"] integerValue];
        self.scorePerClick = [response[@"score_per_click"] integerValue];
        
    }
    return self;
}

@end
