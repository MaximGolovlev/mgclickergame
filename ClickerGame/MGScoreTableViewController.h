//
//  MGScoreTableViewController.h
//  ClickerGame
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGScoreTableViewController : UIViewController

@property (strong, nonatomic) NSMutableDictionary *tableScore;

@end
