//
//  MGScoreTableViewController.m
//  ClickerGame
//
//  Created by Admin on 25.08.15.
//  Copyright (c) 2015 admin. All rights reserved.
//

#import "MGScoreTableViewController.h"
#import "MGMultiColumnCell.h"

@interface MGScoreTableViewController ()

@property (strong, nonatomic) IBOutlet UITableView *levelsTableView;

@end

@implementation MGScoreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self.levelsTableView respondsToSelector:@selector(layoutMargins)])
        self.levelsTableView.layoutMargins = UIEdgeInsetsZero;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.tableScore[@"levels"] count] + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"statCell";
    
    MGMultiColumnCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    [cell fillWithData:self.tableScore forIndexPath:indexPath];
    
    if([cell respondsToSelector:@selector(layoutMargins)])
        cell.layoutMargins = UIEdgeInsetsZero;

    return cell;
}

@end
